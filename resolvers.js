import { employees } from "./db.js";

const rejectIf = (condition, code) => {
    if(condition) throw new Error(code)
}

const resolvers = {
    Query: {
        employees: () => employees.findAll() 
    },
    Mutation: {
        addEmployee: async (_root, {input}, _context) => {
            return employees.create(input);
        }, 
        updateEmployee: async (_root, {input}, _context) => {
            const employee = await employees.findById(input.id)
            rejectIf(!employee, 'NOT_FOUND')
            return await employees.update({
                ...employee,
                ...input
            });
        }, 
    },
}

export default resolvers;