# EmployeePortal

## What does this repo do ?
This is a simple sample repo created for demo purposes to manage employee information 

## Getting started

# Pre-requisites for running this project 
  - Node version manager - https://github.com/nvm-sh/nvm

# How do I run the project ?
```
    - nvm install 17
    - nvm use 17
    - npm install --legacy-peer-deps
    - npm start 
```
# Uses apollo-express-server to host a graphql server with sample queries

## Queries 
# List all employees 
```
query EmployeeQuery {
  employees {
    id
    firstName
    lastName
    email
    phoneNumber
    address
  }
}
```

# Add Employee 
```
const variables = {
  "input": {
      "firstName": "test",
        "lastName": "user",
        "email": "name@yourcompany11.com",
        "phoneNumber": "8787687687",
        "address": null 
  }
}

mutation AddEmployee($input:AddEmployeeInput) { 
  addEmployee(input: $input) {
    id
    firstName
    lastName
    email
    phoneNumber
    address
  }
}
```

# Update Employee
```
const variables = {
  "input": {
        "email": "name@yourcompany11.com",
        "phoneNumber": "8787687687",
        "address": "New Street address" 
  }
}

mutation($input: UpdateEmployeeInput) {
  updateEmployee(input: $input) {
    id
    firstName
    lastName
    email
    phoneNumber
    address
  }
}

```