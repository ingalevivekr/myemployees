import { ApolloServer, gql } from "apollo-server-express";
import { makeExecutableSchema } from "graphql-tools";
import {constraintDirective, constraintDirectiveTypeDefs} from "graphql-constraint-directive";
import express from "express";
import { readFile } from "fs/promises";

import resolvers from "./resolvers.js";
const PORT = process.env.PORT || 9000;

const formatError = (error) => {
  const { message, path } = error;
  console.log(`[GRAPHQL ERROR]: ${JSON.stringify(error)}`);
  return {
    message,
    path,
  };
};

const start = async () => {
  const app = express();
  const typeDefs = await readFile("./schema.graphql", {
    encoding: "utf-8",
  });

  let schema = makeExecutableSchema({
    typeDefs: [constraintDirectiveTypeDefs, typeDefs],
    resolvers,
    schemaDirectives: { constraint: constraintDirective() },
  });
  schema = constraintDirective()(schema)

  const apolloServer = new ApolloServer({ schema, formatError });
  await apolloServer.start();
  apolloServer.applyMiddleware({ app, path: "/graphql" });

  app.listen({ port: PORT }, () => {
    console.log(`Express Server Started on http://localhost:${PORT}`);
    console.log(`Apollo Server Started on http://localhost:${PORT}/graphql`);
  });
};

start();
